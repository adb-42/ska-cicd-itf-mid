#!/bin/bash

FILE=OpenSSHPorts.mak

# if [ "$1" == "close" ]; then
#     kill $2;
# fi

if [ ! -f "$FILE" ]; then
    touch OpenSSHPorts.mak
    echo "SSH_PORT_6443_PROCESS_ID=" >> OpenSSHPorts.mak
    echo "SSH_PORT_2234_PROCESS_ID=" >> OpenSSHPorts.mak
    echo "SSH_PORT_8000_PROCESS_ID=" >> OpenSSHPorts.mak
    echo "SSH_PORT_8080_PROCESS_ID=" >> OpenSSHPorts.mak
fi

ssh -N -L $1:$2:$3 pi@mid-itf.duckdns.org -p 2322 &
PID=$!
sed -i -e "s/SSH_PORT_$1_PROCESS_ID=.*/SSH_PORT_$1_PROCESS_ID=$PID/" OpenSSHPorts.mak
rm OpenSSHPorts.mak-e || true

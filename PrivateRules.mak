# If NO_COPY exists, don't copy these PrivateRules into the .deploy-minikube 
# sub-directory - comment out if you want them to be copied over. 
# The *_REPLACEME variables are replaced with values from here as soon as 
# they're copied over
NO_COPY=True

USE_CACHE=yes

# This value
MEM_MINIKUBE=8192
#  will replace this value
MEM=MEM_MINIKUBE_REPLACEME
DISK=DISK_MINIKUBE_REPLACEME

# This value
DISK_MINIKUBE=40000
# will replace this value

# Minikube cluster setup variables
CPUS = 4

# DRIVER = docker
# RUNTIME = docker
ADDONS:=$(ADDONS) --addons=ingress-dns --disk-size='DISK_MINIKUBE_REPLACEMEmb'

time-now:
	$(info $(shell date +"%T"))

info:
	$(info ######## INFO START)
	$(info HAPROXY_IP: $(HAPROXY_IP))
	$(info ADDONS: $(ADDONS))
	$(info RAM: $(MEM_COLIMA) GiB; $(MEM_MINIKUBE) MiB to Minikube; MEM => $(MEM))
	$(info DISK: $(DISK_COLIMA) GiB; $(DISK_MINIKUBE) MiB to Minikube)
	$(info INFO END ##########)
	$(info KUBE_NAMESPACE: $(KUBE_NAMESPACE))
	$(info K8S_CHART_PARAMS: $(K8S_CHART_PARAMS))

-include ./OpenSSHPorts.mak

HAPROXY_IP := $(shell hostname -I | cut -f1 -d' ')

